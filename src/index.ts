import log from "@ajar/marker";

function random() : number;
function random(float : boolean) : number;
function random(float : boolean ,max : number): number;
function random(min :number, max : number) : number;
function random(float  : boolean, min :number, max : number) : number;

function random(...args : any) {
    const length = args.length;
    switch(length) {
        case 0 :{
            return Math.floor(Math.random() *2);
        }
        case 1: {
                if (args[0] === false){
                    return Math.floor(Math.random() *2);
                } else {
                    return Math.random();
                }
            }
        case 2: {
                if (args[0] === false){
                    return Math.floor(Math.random() * args[1]);
                } else {
                    return Math.random() * args[1];
                }
            }
        case 3: {
            if (args[0] === false){
                return Math.floor(Math.random() * (args[2] - args[1]) + args[1]);
            } else {
                return Math.random() * (args[2] - args[1]) + args[1];
            }
        }
    }
}

const num1 = random(); // 0 ~ 1 | integer
const num2 = random(true); // 0 ~ 1 | float
const num3 = random(false, 6); // 0 ~ 6 | integer,
const num4 = random(false, 2, 6); // 2 ~ 6 | integer
const num5 = random(true, 6); // 0 ~ 6 | float
const num6 = random(true, 2, 6); // 2 ~ 6 | float

console.log({ num1, num2, num3, num4, num5, num6 });